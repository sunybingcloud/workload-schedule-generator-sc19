# workload-schedule-generator

Generates/Modifies the workload schedule for Electron. Can stream the generated schedule to Electron via HTTP or save it to be used for later.
Works with Python 2.7 (Should work with Python3 too)

### Usage:

```
usage: main.py [-h] [-o OUTPUT] [-addr ADDR] [-l LOG] [-e] input_file

positional arguments:
  input_file            Workload file in JSON format OR Existing schedule to stream from

optional arguments:
  -h, --help            show this help message and exit
  -o OUTPUT, --output OUTPUT
                        Optional output file
  -addr ADDR, --addr ADDR
                        IP:Port of the host where HTTP requests will be sent
  -l LOG, --log LOG     Set the logging level (Supported values: DEBUG, INFO,
                        WARNING, ERROR, CRITICAL)
  -e, --existing        Just stream from existing schedule in input_file.
```

* Basic command to run the script:
```
$ python main.py <workloads_JSON_file> -addr <IP:Port>
```

Ex.
```
$ python main.py input.json -addr 127.0.0.1:4545
```

* To stream from an already created schedule, use the `-e` flag. In this case, the input_file provided should be a schedule and not sample workload file.

Ex.
`python main.py generated_schedule.txt -addr localhost:4545 -e`


* `-o <OUTPUT_File>` When provided, the generated schedule will also be written to the output file specified.

Ex.
`$ python main.py input.json -addr 127.0.0.1:4545 -o newoutput.txt`


* `-l LOG_LEVEL` Set the log level. Supported values: DEBUG, INFO, WARNING, ERROR, CRITICAL. Default: INFO

Ex.
`$ python main.py input.json -addr 127.0.0.1:4545 -l DEBUG`

#### Changing configuration:

Currently, 4 sections are supported - Power intensive, non power intensive, dense and sparse.
For each section, you can configure the

1. Number of jobs

2. min and max sleep time within that section

3. min and max number of instances

4. proportion of high and low power instances

To change any of these configuration, edit the `section_specs` dict within `sections.py` file.

---

The high power consuming tasks are : `cryptography`, `video-encoding`, `xalan`, `sunflow`, 
`dgemm`, `lusearch`, `tomcat`.
To add/remove high power consuming tasks, edit the `high_power_consuming_tasks` dict within `sections.py`.

To add a new section, add the section name in `sections_list` of `sections.py` and add specifications of that section in `section_specs` dict.

---

#### Support for custom info/tolerance values

We've added the `--custom <FILE_NAME>` parameter to support custom tolerance values or any other custom properties of tasks.
The custom task info specified in this file OVERRIDES the default configuration present in input workload file.

Format of the custom file:
```json
    {
        "Section Name":
        [
            {"name": "NAME1", "tolerance": 0.5},
            {"name": "NAME2", "tolerance": 0.7},
             ...
    
        ],
        "Section Name 2":
        [
            {...},
            {...}
        ]
    }
``` 

As of now, we're supporting only custom tolerance values.

Sample command:

```bash
$ python main.py input.json -addr 127.0.0.1:4545 --custom custom_tolerance.json
```

Please refer to custom_tolerance.json for a sample.

---
