#!/usr/bin/env python

import sys
import argparse
import logging
import utils
import schedule
import sections
import http


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("input_file", help="Workload file in JSON format OR Existing schedule "
                                           "(if --existing flag is set", type=str)
    parser.add_argument("-o", "--output", help="Optional output file", type=str)
    parser.add_argument("-addr", "--addr", help="IP:Port of the host where HTTP requests will be sent",
                        type=str, default=None)
    parser.add_argument("-l", "--log", help="Set the logging level (Supported values: DEBUG, INFO,\
    WARNING, ERROR, CRITICAL)", type=str, default="INFO")
    parser.add_argument("-e", "--existing", help="Just stream from existing schedule in input_file.",
                        action='store_true')
    parser.add_argument("-c", "--custom", help="JSON file containing overriding custom workload info"
                                               "(Such as custom tolerance values)", type=str, default=None)
    args = parser.parse_args()

    return args


def display_menu():
    selected_sections = []
    sections_list = sections.sections_list
    n = len(sections_list)
    to_stream = True
    to_store = False

    while True:
        # Display selected sections
        print("\nSelected sections:")
        print("\n".join(selected_sections) if len(selected_sections) != 0 else "<nil>")
        print("\n")
        
        # Display menu
        print("Select one of the following:")
        index = 1
        for section in sections_list:
            print("%d. %s" % (index, section))
            index += 1
        print("%d. Repeat" % index)
        index += 1
        print("%d. Start streaming" % index)
        index += 1
        print("%d. Save and exit" % index)
        index += 1
        print("%d. Exit" % index)

        print("Enter an option:")
        try:
            selected_option = int(input())
        except ValueError or NameError:
            continue

        if 0 < selected_option <= n:
            # One of the sections selected
            selected_sections.append(sections_list[selected_option - 1])
            print("Section added.")

        elif selected_option == n+1:
            # Repeat option selected
            print("Enter number of times to repeat.")
            repeat_n = int(input()) + 1
            selected_sections = selected_sections * repeat_n
            print("Sections repeated.")

        elif selected_option == n+2:
            # Start streaming selected
            return selected_sections, to_stream, to_store

        elif selected_option == n+3:
            # Save schedule and exit
            to_stream = False
            to_store = True
            return selected_sections, to_stream, to_store

        elif selected_option == n+4:
            # Exit
            sys.exit(0)

        else:
            print("Given option was not understood. Please try again.")


def display_short_menu():

    while True:

        # Display menu
        print("Select one of the following:")

        print("1. Just Stream")
        print("2. Just Save")
        print("3. Stream and Save")
        print("4. Exit")

        print("Enter an option:")
        try:
            selected_option = int(input())
        except ValueError or NameError:
            continue

        if selected_option == 1:
            # Just stream
            to_store = False
            to_stream = True
            return to_stream, to_store

        elif selected_option == 2:
            # Just save
            to_stream = False
            to_store = True
            return to_stream, to_store

        elif selected_option == 3:
            # Save and stream
            to_stream = True
            to_store = True
            return to_stream, to_store

        elif selected_option == 4:
            # Exit
            sys.exit(0)

        else:
            print("Given option was not understood. Please try again.")


def set_logging(loglevel):
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        numeric_level = logging.INFO    # Default level

    logging.basicConfig(level=numeric_level)

    # Suppress unnecessary "INFO" level logging by requests library
    logging.getLogger("requests").setLevel(logging.WARNING)


def main():
    args = get_args()

    set_logging(args.log)
    filepath = args.input_file

    to_store = False
    to_stream = True

    if args.existing:
        generated_schedule = utils.parse_schedule(filepath)
        logging.info("Existing schedule loaded.")
        to_stream, to_store = display_short_menu()
    else:
        workload = utils.parse_file(filepath)
        selected_sections, to_stream, to_store = display_menu()

        generated_schedule = schedule.create_schedule(workload, selected_sections)

    if args.custom:
        custom_workload_info = utils.parse_custom_workload(args.custom)
        generated_schedule = schedule.modify_schedule(generated_schedule, custom_workload_info)
        logging.info("Schedule modified according to custom values")

    if to_store or args.output:
        serialized_schedule = schedule.serialize_schedule(generated_schedule)
        utils.write_to_output(serialized_schedule, args.output)

    if to_stream:
        http.post_streaming_schedule(args.addr, generated_schedule)


if __name__ == '__main__':
    main()
