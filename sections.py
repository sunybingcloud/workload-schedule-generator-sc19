
sections_list = ["Power Intensive", "Non Power Intensive", "Sparse Jobs", "Dense Jobs"]

high_power_consuming_tasks = {"cryptography", "video-encoding", "xalan", "sunflow",
                              "dgemm", "lusearch", "tomcat"}

DEFAULT_MIN_SLEEP_TIME = 5
DEFAULT_MAX_SLEEP_TIME = 300

DEFAULT_MIN_INSTANCES = 200
DEFAULT_MAX_INSTANCES = 300

DEFAULT_JOBS_IN_SECTION = 5

section_specs = {
    "Power Intensive": {
        "jobs": DEFAULT_JOBS_IN_SECTION,
        "min_sleep_time": DEFAULT_MIN_SLEEP_TIME,
        "max_sleep_time": DEFAULT_MAX_SLEEP_TIME,
        "low_power_proportion": 5,
        "high_power_proportion": 12,
        "min_instances": DEFAULT_MIN_INSTANCES,
        "max_instances": DEFAULT_MAX_INSTANCES
    },

    "Non Power Intensive": {
        "jobs": DEFAULT_JOBS_IN_SECTION,
        "min_sleep_time": DEFAULT_MIN_SLEEP_TIME,
        "max_sleep_time": DEFAULT_MAX_SLEEP_TIME,
        "low_power_proportion": 20,
        "high_power_proportion": 3,
        "min_instances": DEFAULT_MIN_INSTANCES,
        "max_instances": DEFAULT_MAX_INSTANCES
    },

    "Sparse Jobs": {
        "jobs": DEFAULT_JOBS_IN_SECTION,
        "min_sleep_time": 200,
        "max_sleep_time": 300,
        "low_power_proportion": -1,  # Proportion doesn't matter. Can be selected anyhow
        "high_power_proportion": -1,
        "min_instances": 1,
        "max_instances": 50
    },

    "Dense Jobs": {
        "jobs": DEFAULT_JOBS_IN_SECTION,
        "min_sleep_time": 1,
        "max_sleep_time": 120,
        "low_power_proportion": -1,  # Proportion doesn't matter. Can be selected anyhow
        "high_power_proportion": -1,
        "min_instances": 400,
        "max_instances": 600
    }
}
