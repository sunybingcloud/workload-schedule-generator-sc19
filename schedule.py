import random
import logging
import utils
import sections
import copy

# Stores list of low-power and high-power tasks respectively
# Kept as global variables to retain their values for the duration of program.
low_power_tasks = []
high_power_tasks = []


# Divides the sample task list into low-power and high-power tasks
def bifurcate_tasks(tasks):
    global low_power_tasks
    global high_power_tasks

    for task in tasks:
        if task["name"] in sections.high_power_consuming_tasks:
            high_power_tasks.append(task)
        else:
            low_power_tasks.append(task)


# Calculates and sets correct number of instances for a given task based on its index
def set_instances_for_task(task, task_idx, n_instances, n_tasks):
    instances_for_task = n_instances // n_tasks     # Instances are equally divided
    if task_idx < (n_instances % n_tasks):  # Any extra instances are equally given to first few tasks
        instances_for_task += 1

    task["inst"] = instances_for_task


# Generates tasks for a job based on number of instances it has
def create_tasks_for_job(tasks, n_instances):
    tasks_in_job = []

    # No of tasks in job should not be greater than number of instances
    n_tasks = random.randint(1, min(len(tasks), n_instances))
    selected_tasks = random.sample(tasks, n_tasks)

    # Make a deep copy of task JSON and set no. of instances for all tasks in the job
    for i in range(len(selected_tasks)):
        selected_task = copy.deepcopy(selected_tasks[i])
        # Reason for deep copy: Dict of one task can appear in multiple jobs.
        # When we change number of instances for one task, we don't want the instances to
        # get changed for the same task in other jobs (Since dict is passed by reference)
        set_instances_for_task(selected_task, i, n_instances, n_tasks)

        tasks_in_job.append(selected_task)

    return tasks_in_job


# Returns shorthand string for a task. Used for debugging
def task_string(task):
    return ("Name: %s\t\t Instances: %d\t High power: %r" %
            (task["name"], task["inst"], task["name"] in sections.high_power_consuming_tasks))


def create_job(tasks, low_power_proportion, high_power_proportion,
               min_instances, max_instances):
    global low_power_tasks
    global high_power_tasks

    tasks_in_job = []
    if low_power_proportion == -1 or high_power_proportion == -1:
        # Sparse and dense sections case

        n_instances = random.randint(min_instances, max_instances)
        tasks_in_job = create_tasks_for_job(tasks, n_instances)
    else:
        # Power intensive and non power intensive case
        if not low_power_tasks or not high_power_tasks:     # Preventive check
            # Divide the tasks in low-power and high-power, populate the global variables
            bifurcate_tasks(tasks)

        total_proportion = low_power_proportion + high_power_proportion

        # Total number of instances for the job should be a multiple of "total_proportion"
        # in the range of [min_instances, max_instances] of current section. This ensures that
        # number of low power instances and high power instances are in proportion
        n_instances = random.choice([x for x in range(min_instances, max_instances)
                                    if x % total_proportion == 0])

        # Find actual number of low power and high power instances
        low_power_instances = low_power_proportion * n_instances // total_proportion
        high_power_instances = high_power_proportion * n_instances // total_proportion

        # Create low power tasks and high power tasks respectively
        low_power_tasks_in_job = create_tasks_for_job(low_power_tasks, low_power_instances)
        high_power_tasks_in_job = create_tasks_for_job(high_power_tasks, high_power_instances)

        tasks_in_job.extend(low_power_tasks_in_job)     # Add them to the job
        tasks_in_job.extend(high_power_tasks_in_job)

    return tasks_in_job


def get_interval_secs(min_sleep_time, max_sleep_time):
    interval_secs = random.randint(min_sleep_time, max_sleep_time)
    return interval_secs


def create_section(tasks, section_spec):

    # We expect "tasks" to be a list of tasks to be run
    assert(isinstance(tasks, list))

    # Retrieve all the properties of a section
    n_jobs = section_spec["jobs"]
    min_sleep_time = section_spec["min_sleep_time"]
    max_sleep_time = section_spec["max_sleep_time"]
    low_power_proportion = section_spec["low_power_proportion"]
    high_power_proportion = section_spec["high_power_proportion"]
    min_instances = section_spec["min_instances"]
    max_instances = section_spec["max_instances"]

    output = []
    # Create the required number of jobs in the section, adding sleep times in between
    for i in range(n_jobs):
        tasks_in_job = create_job(tasks, low_power_proportion, high_power_proportion,
                                  min_instances, max_instances)
        logging.debug("Job %d:" % (i+1))
        logging.debug("\n"+"\n".join([task_string(x) for x in tasks_in_job])+"\n")

        output.append(tasks_in_job)

        interval_secs = get_interval_secs(min_sleep_time, max_sleep_time)
        logging.debug("wait %d\n" % interval_secs)
        output.append(interval_secs)

    return output


# High-level function which accepts a list of user-selected sections and creates those
# sections by calling underlying functions
def create_schedule(workload, selected_sections):
    output = []

    for section_name in selected_sections:
        output.append("%s" % section_name)
        logging.debug("SECTION:- %s" % section_name)

        section_spec = sections.section_specs[section_name]
        generated_section = create_section(workload, section_spec)
        output.extend(generated_section)

        output.append("End %s" % section_name)

    return output


def modify_schedule(schedule, custom_workload_info):

    # Currently, custom_workload_info contains nothing but section-wise tolerance values for each task.
    """
    Format of custom_workload_info:
    {
        "Section Name": {
            "task_name": task_tolerance,
            "task_name2": task_tolerance2,
            ...
        },
        "Section Name 2": {
            "task_name": task_tolerance,
            "task_name2": task_tolerance2,
            ...
        }
    }
    """

    current_section = ""

    for idx, item in enumerate(schedule):

        if isinstance(item, str):
            # It means "item" is a section title
            current_section = item

        elif isinstance(item, list):
            # It means "item" is a job

            if current_section not in custom_workload_info:
                # custom_workload_info doesn't have any information about the section current job is in. Nothing to do.
                continue

            for task in item:
                task_name = task["name"]

                if task_name in custom_workload_info[current_section]:
                    task["tolerance"] = custom_workload_info[current_section][task_name]

    return schedule


def serialize_schedule(schedule):
    return '\n'.join([str(x) if isinstance(x, int) or isinstance(x, str) else utils.dump(x) for x in schedule])
